## Setup macOS environment for SSAGES installation 

Install [anaconda](https://www.anaconda.com/download/#macos) containing python version 2.7
Note that PATH variable to anaconda2 will be automatically added to your ~/.bash_profile after anaconda2 installation is complete
```bash
    export PATH="/PATH_TO_ANACONDA/anaconda2/bin:$HOME/bin:$PATH"
```

Add Path variable in ~/.bashrc or ~/.bash_profile
```bash
    export PATH="/usr/local/bin:/usr/bin:/PATH_TO_ANACONDA/anaconda2/bin:$HOME/bin:$PATH"
```

Open a new terminal window. 
Install gcc-4.9
```bash
    $ brew install gcc49
```

Modify ~/.bashrc or ~/.bash_profile to change gcc-4.9 to be the default compiler
```bash
    export HOMEBREW_CC=gcc-4.9
    export HOMEBREW_CXX=g++-4.9
	export CC=/usr/local/bin/gcc-4.9
    export CXX=/usr/local/bin/g++-4.9
```

Open a new terminal window.
Replace default openmpi in Mac built using Clang with a new one
```bash
    $ brew reinstall openmpi --build-from-source
```

Check that openmpi version is 1.8 or higher and that it is built using gcc compilers by typing in the terminal,
```bash
    $ ompi_info
```
The terminal output should contain something like
```bash
     C compiler: gcc-4.9
     C compiler absolute: /usr/local/Homebrew/Library/Homebrew/shims/mac/super/gcc-4.9
     C compiler family name: GNU
     C compiler version: 4.9.4
     C++ compiler: g++-4.9
     C++ compiler absolute: /usr/local/Homebrew/Library/Homebrew/shims/mac/super/g++-4.9
```

## Now we are ready to build SSAGES. Here is an example for building SSAGES with LAMMPS

Get the source code for SSAGES
```bash
    $ git clone https://github.com/MICCoM/SSAGES-public.git
```

Inside SSAGES directory, type the following commands:
```bash
    $ mkdir build/
    $ cd build/
    $ cmake .. -DLAMMPS=YES
    $ make
```
This will have SSAGES automatically download LAMMPS so that both SSAGES and LAMMPS will be built together.

Once SSAGES is successfully built, there will be an executable file named "ssages" inside "build" directory.
When you try to run ssages by typing in the terminal,
```bash
    $ ./ssages
```
there will be an error message,
```bash
    dyld: Library not loaded: ../liblammps_mpi.so
       Referenced from: /PATH_TO_SSAGES/build/./ssages
       Reason: image not found
    Abort trap: 6
```
To fix this error, first copy liblammps_mpi.so to the build directory:
```bash
    $ cp hooks/lammps/lammps-download-prefix/src/lammps-download/src/liblammps_mpi.so .
``` 

Then type,
```bash
    $ install_name_tool -change ../liblammps_mpi.so /PATH_TO_SSAGES/build/hooks/lammps/lammps-download-prefix/src/lammps-download/src/liblammps_mpi.so ssages
``` 

Verfify that the library path is correctly set up,
```bash
    $ otool -L ssages
    ssages:
	       /Users/liza/SSAGES-public/build/hooks/lammps/lammps-download-prefix/src/lammps-download/src/liblammps_mpi.so (compatibility version 0.0.0, current version 0.0.0)
	       /usr/local/opt/open-mpi/lib/libmpi.40.dylib (compatibility version 51.0.0, current version 51.2.0)
	       /usr/local/opt/gcc@4.9/lib/gcc/4.9/libstdc++.6.dylib (compatibility version 7.0.0, current version 7.20.0)
	       /usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 1252.50.4)
	       /usr/local/lib/gcc/4.9/libgcc_s.1.dylib (compatibility version 1.0.0, current version 1.0.0)
``` 

Check that you can run one of the examples,
```bash
    cd /PATH_TO_SSAGES/Examples/User/Meta/Single_Atom
	../../../../build/ssages Meta.json 
``` 